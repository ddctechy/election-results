# **Election Results:**

Run docker-compose file located in the root directory  of the project locally then you can access the below url's


## **Front-End (UI):**

URL to Upload Results (Import the raw file):
http://localhost:8081

URL to see results:
http://localhost:8081/results


If you want check them from postman client use below urls

### API to Upload Results (Import the raw file):

#### POST Request:

http://localhost:8080/v1/api/elections/upload-results

### Constituency API :

#### GET Request:

http://localhost:8080/v1/api/elections/get-constituency-results


### Total Results API :

#### GET Request:

http://localhost:8080/v1/api/elections/get-total-results


## **Packaging:**

Application is packaged using docker, you can run docker-compose.yaml file located in root directory of the project locally to build the application in docker container.

## **CI:**

Script for CI jobs has been added in .gitlab-ci.yml 