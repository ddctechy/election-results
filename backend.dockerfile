FROM gradle:7.4.2-jdk17-alpine AS build

COPY --chown=gradle:gradle . /home/results
WORKDIR /home/results
RUN gradle build --no-daemon --console=plain

FROM openjdk:17-alpine

EXPOSE 8080

RUN mkdir /app

COPY --from=build /home/results/build/libs/*.jar /app/

CMD ["java", "-jar","/app/results.jar"]
