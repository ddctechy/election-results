package com.elections.results.service;

import com.elections.results.Exception.ElectionResultsException;
import com.elections.results.domain.ElectionResults;
import com.elections.results.repository.ElectionResultRepository;
import com.elections.results.repository.ResultPerConstituencyRepository;
import com.elections.results.repository.ResultPerPartyRepository;
import com.elections.results.service.mapper.ResultPerPartySummaryMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigInteger;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElectionResultsService.class)
@AutoConfigureMockMvc
public class ElectionResultsServiceTest {

    @MockBean
    ElectionResultRepository electionResultRepository;

    @MockBean
    ResultPerConstituencyRepository resultPerConstituencyRepository;

    @MockBean
    ResultPerPartyRepository resultPerPartyRepository;

    @MockBean
    ResultPerPartySummaryMapper resultPerPartySummaryMapper;

    @Autowired
    ElectionResultsService electionResultsService;

    @Autowired
    private MockMvc mockMvc;

    protected MockMvc getMockMvc(){
        return mockMvc;
    }

    @Test
    public void testConvertToElectionResult(){

        String r = "Cardiff\\, West, 11014, C, 17803, L, 4923, UKIP, 2069, LD";
        List<ElectionResults> electionResults = electionResultsService.convertToElectionResult(r);

        assert(electionResults.size()==4);
        assert (electionResults.get(0).getConstituency().equals("Cardiff, West"));
    }

    @Test
    public void testConvertToElectionResultWithoutComma(){

        String r = "Cardiff West, 11014, C, 17803, L, 4923, UKIP, 2069, LD";
        List<ElectionResults> electionResults = electionResultsService.convertToElectionResult(r);

        assert(electionResults.size()==4);
        assert (electionResults.get(0).getConstituency().equals("Cardiff West"));
    }

    @Test
    public void testConvertToElectionResultWithDifferentSymbols(){

        String r = "Cardiff& West, 11014, C, 17803, L, 4923, UKIP, 2069, LD";
        List<ElectionResults> electionResults = electionResultsService.convertToElectionResult(r);

        assert(electionResults.size()==4);
        assert (electionResults.get(0).getConstituency().equals("Cardiff& West"));
    }

    @Test
    public void testConvertToElectionResultWithDuplicateValues(){

        String r = "Cardiff\\, West, 11014, C, 11014, C, 17803, L, 4923, UKIP, 2069, LD, 2969, LD";
        List<ElectionResults> electionResults = electionResultsService.convertToElectionResult(r);

        assert(electionResults.size()==4);
        assert (electionResults.get(0).getConstituency().equals("Cardiff, West"));
    }

    @Test
    public void testConvertToElectionResultFail(){

        String r = "Cardiff, West, 11014, C, 17803, L, 4923, UKIP, 2069, LD";
        try {
            electionResultsService.convertToElectionResult(r);
        } catch (ElectionResultsException e){
            assert (e.getMessage().equals("Input file has error data : For input string: \"West\""));
        }
    }

    @Test
    public void testGetVotePercent() {
        assert (electionResultsService.getVotePercent(776655544.00, new BigInteger("37665587")).equals("4.85"));
    }

    @Test
    public void testGetValidParty() {
        assert (electionResultsService.getValidParty("SNP").equals("SNP"));
    }

    @Test
    public void testGetValidPartyFail() {

        try {
            electionResultsService.getValidParty("SNP1");
        } catch (ElectionResultsException e){
            assert (e.getMessage().equals("Party Code is invalid : SNP1"));
        }
    }


}
