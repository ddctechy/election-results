package com.elections.results.controller;

import com.elections.results.service.ElectionResultsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ResultsController.class)
@AutoConfigureMockMvc
public class ResultsControllerTest {

    @MockBean
    ElectionResultsService electionResultsService;
    @Autowired
    private MockMvc mockMvc;

    protected MockMvc getMockMvc(){
        return mockMvc;
    }

    @Before
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testUploadElectionsResults() throws Exception {
        MockMultipartFile jsonFile = new MockMultipartFile("test.json", "", "application/json", "Cardiff West, 11014, C, 17803, L, 4923, UKIP, 2069, LD".getBytes());
        getMockMvc().perform(MockMvcRequestBuilders.multipart("/v1/api/elections/upload-results")
                        .file("file", jsonFile.getBytes())
                        .characterEncoding("UTF-8"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetConstituencyResults() throws Exception {

        getMockMvc().perform(get("/v1/api/elections/get-constituency-results")
                        .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }

    @Test
    public void testGetTotalResults() throws Exception {

        getMockMvc().perform(get("/v1/api/elections/get-total-results")
                        .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }
}

