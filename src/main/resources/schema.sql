DROP VIEW IF EXISTS RESULT_PER_PARTY;
DROP VIEW IF EXISTS RESULT_PER_CONSTITUENCY;
DROP TABLE IF EXISTS ELECTION_RESULTS;


CREATE TABLE ELECTION_RESULTS (
  id INT PRIMARY KEY,
  constituency VARCHAR(250) NOT NULL,
  party_code VARCHAR(250) NOT NULL,
  votes INT NOT NULL,
  CONSTRAINT UC_CP UNIQUE(constituency, party_code)
);

create view RESULT_PER_CONSTITUENCY as
SELECT b.id, b.constituency, b.party_code, b.votes, TOTAL
FROM (
  select CONSTITUENCY, sum (VOTES) as TOTAL, max(VOTES) as MAX from ELECTION_RESULTS group by CONSTITUENCY
) AS t
INNER JOIN ELECTION_RESULTS as b
ON t.MAX = b.VOTES;

create view RESULT_PER_PARTY as
  select Row_number() OVER(ORDER BY party_code ASC) AS id, party_code, total_mps, total_votes from
  (select count(cr.party_code) as total_mps, cr.party_code as party from RESULT_PER_CONSTITUENCY cr group by cr.party_code) as t1
  RIGHT JOIN
  (select party_code, sum (VOTES) as total_votes  from ELECTION_RESULTS group by party_code) as t2
  ON t1.party = t2.party_code;

--create view RESULT_PER_PARTY as
--  select Row_number() OVER(ORDER BY party_code ASC) AS id, party_code, sum (VOTES) as total_votes , total_mps  from (select count(cr.party_code) as total_mps, cr.party_code as party from RESULT_PER_CONSTITUENCY cr, RESULT_PER_PARTY pr where cr.party_code = pr.party_code) as t RIGHT JOIN ELECTION_RESULTS as r on r.party_code = t.party group by party_code;
--create view RESULT_PER_PARTY as
--select Row_number() OVER(ORDER BY party_code ASC) AS id, party_code, count(CONSTITUENCY) as total_mps, sum (VOTES) as total_votes  from ELECTION_RESULTS group by (party_code);
