package com.elections.results.service.mapper;

import com.elections.results.domain.PartyCode;
import com.elections.results.domain.ResultPerParty;
import com.elections.results.model.ResultPerPartySummary;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ResultPerPartySummaryMapper implements EntityMapper<ResultPerPartySummary, ResultPerParty>{


    /**
     * @param dto
     * @return
     */
    @Override
    public ResultPerParty toEntity(ResultPerPartySummary dto) {
        return new ResultPerParty();
    }

    /**
     * @param entity
     * @return
     */
    @Override
    public ResultPerPartySummary toDto(ResultPerParty entity) {
        return ResultPerPartySummary.newResultPerPartySummary()
                .partyName(PartyCode.valueOf(entity.getPartyCode().toUpperCase()).getPartyName())
                .totalMPs(Optional.ofNullable(entity.getTotalMPs()).orElse(0))
                .totalVotes(entity.getTotalVotes())
                .build();
    }
}
