package com.elections.results.service;

import com.elections.results.Exception.ElectionResultsException;
import com.elections.results.domain.*;
import com.elections.results.model.ElectionResultsSummary;
import com.elections.results.model.ResultPerPartySummary;
import com.elections.results.repository.ElectionResultRepository;
import com.elections.results.repository.ResultPerConstituencyRepository;
import com.elections.results.repository.ResultPerPartyRepository;
import com.elections.results.service.mapper.ResultPerPartySummaryMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Transactional
public class ElectionResultsService {

    private final ElectionResultRepository electionResultRepository;

    private final ResultPerConstituencyRepository resultPerConstituencyRepository;

    private final ResultPerPartyRepository resultPerPartyRepository;

    private final ResultPerPartySummaryMapper resultPerPartySummaryMapper;

    @Autowired
    public ElectionResultsService(ElectionResultRepository electionResultRepository,
                                  ResultPerConstituencyRepository resultPerConstituencyRepository,
                                  ResultPerPartyRepository resultPerPartyRepository,
                                  ResultPerPartySummaryMapper resultPerPartySummaryMapper) {
        this.electionResultRepository = electionResultRepository;
        this.resultPerConstituencyRepository = resultPerConstituencyRepository;
        this.resultPerPartyRepository = resultPerPartyRepository;
        this.resultPerPartySummaryMapper = resultPerPartySummaryMapper;
    }

    public void processElectionResults(MultipartFile resultFile) throws ElectionResultsException {

        try {
            InputStream inputStream = resultFile.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            List<ElectionResults> electionResultsList = bf.lines()
                    .filter(s -> !s.isBlank())
                    .map(this::convertToElectionResult)
                    .toList()
                    .stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

            saveElectionResults(electionResultsList);

        } catch (IOException e) {
            throw new ElectionResultsException("Unable to process election results: " + e.getMessage());
        }
    }

    protected List<ElectionResults> convertToElectionResult(String line) {

        String reg = "(?<!\\\\)" + Pattern.quote(",");
        String[] r = line.split(reg);

        String constituency = getConstituency(r[0]);

        HashMap<String, BigInteger> partyVotes = new HashMap<>();
        try {
            for (int i = 1; i < (r.length - 1); i++) {

                BigInteger votes = new BigInteger(r[i++].trim());
                String party = getValidParty(r[i].trim());

                partyVotes.put(party, votes);
            }
        } catch (NumberFormatException e) {
            throw new ElectionResultsException("Input file has error data : " + e.getMessage());
        }

        return partyVotes.entrySet()
                .stream()
                .map(ele -> new ElectionResults(constituency, ele.getValue(), ele.getKey()))
                .toList();

    }

    private String getConstituency(String s) {
        String constituency = s.trim();
        if (constituency.contains("\\,")) {
            constituency = constituency.replace("\\", "");
        }
        return constituency;
    }

    protected String getValidParty(String partyCode) {

        try {
            PartyCode.valueOf(partyCode.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new ElectionResultsException("Party Code is invalid : " + partyCode);
        }
        return partyCode;
    }

    @Transactional
    public void saveElectionResults(List<ElectionResults> electionResultsList) {

        List<ElectionResults> erNewList = new ArrayList<>();
        for (ElectionResults er : electionResultsList) {

            Optional<ElectionResults> erDB = electionResultRepository.getElectionResultsByUniqueConstraint(er.getConstituency(), er.getPartyCode());

            if (erDB.isPresent()) {
                erDB.get().setVotes(er.getVotes());
                erNewList.add(erDB.get());
            } else {
                erNewList.add(er);
            }
        }

        electionResultRepository.saveAll(erNewList);
    }

    @Transactional
    public String getElectionResults() {

        List<ResultPerConstituency> cr
                = resultPerConstituencyRepository.findAll();

        List<ElectionResults> er = electionResultRepository.findAll();

        double finalTv = getTotalVotes(cr);
        List<ElectionResultsSummary> resultsSummaryList = cr.stream()
                .map(crEle ->
                        er.stream().
                                filter(erEle -> crEle.getId().equals(erEle.getId()))
                                .map(erEle ->
                                        ElectionResultsSummary.newElectionResultsSummary()
                                                .id(crEle.getId())
                                                .constituency(crEle.getConstituency())
                                                .winner(PartyCode.valueOf(crEle.getPartyCode().toUpperCase()).getPartyName())
                                                .votesPercentage(getVotePercent(finalTv, crEle.getTotalVotes()))
                                                .party(er.stream()
                                                        .filter(erElem -> erEle.getConstituency().equals(erElem.getConstituency()))
                                                        .map(erElem ->
                                                                Party.newParty()
                                                                        .partyName(PartyCode.valueOf(erElem.getPartyCode().toUpperCase()).getPartyName())
                                                                        .votes(erElem.getVotes())
                                                                        .build())
                                                        .toList())
                                                .build())
                                .toList())
                .flatMap(Collection::stream)
                .toList();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.registerModule(new Jdk8Module());

        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            mapper.writeValue(out, resultsSummaryList);
        } catch (IOException e) {
            throw new ElectionResultsException("Unable to make Json format in getting ElectionResults: " + e.getMessage());
        }

        return out.toString();
    }

    private double getTotalVotes(List<ResultPerConstituency> cr) {
        double tv = 0.00;
        for (ResultPerConstituency crEle : cr) {
            tv = tv + crEle.getTotalVotes().doubleValue();
        }
        return tv;
    }

    @Transactional
    public String getTotalResultsPerParty() {

        List<ResultPerPartySummary> pr = new ArrayList<>(resultPerPartyRepository.findAll()
                .stream()
                .map(resultPerPartySummaryMapper::toDto)
                .toList());

        if (pr.size() < PartyCode.values().length) {

            Queue<PartyCode> partyCodes = new ConcurrentLinkedQueue<>(Arrays.stream(PartyCode.values()).toList());

            for (PartyCode pc : PartyCode.values()) {
                for (ResultPerPartySummary prEle : pr) {
                    if (prEle.getPartyName().equalsIgnoreCase(pc.getPartyName())) {
                        partyCodes.remove(pc);
                    }
                }
            }

            List<ResultPerPartySummary> resultPerPartySummaryList = partyCodes.stream().map(pcEle -> ResultPerPartySummary.newResultPerPartySummary()
                    .partyName(pcEle.getPartyName())
                    .totalMPs(0)
                    .totalVotes(0)
                    .build()).toList();
            pr.addAll(resultPerPartySummaryList);
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.registerModule(new Jdk8Module());

        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            mapper.writeValue(out, pr);
        } catch (IOException e) {
            throw new ElectionResultsException("Unable to make Json format in getting TotalResultsPerParty: " + e.getMessage());
        }

        return out.toString();
    }

    protected String getVotePercent(Double totalVotes, BigInteger votes) {
        return new DecimalFormat( "#.00" ).format(votes.doubleValue() * 100 / totalVotes);
    }
}
