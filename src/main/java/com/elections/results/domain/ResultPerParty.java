package com.elections.results.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "RESULT_PER_PARTY")
public class ResultPerParty {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "party_code")
    private String partyCode;

    @Column(name = "total_votes")
    private Integer totalVotes;

    @Column(name = "total_mps")
    private Integer totalMPs;

    public ResultPerParty() {
    }

    public ResultPerParty(Long id, String partyCode, Integer totalVotes, Integer totalMPs) {
        this.id = id;
        this.partyCode = partyCode;
        this.totalVotes = totalVotes;
        this.totalMPs = totalMPs;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public Integer getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(Integer totalVotes) {
        this.totalVotes = totalVotes;
    }

    public Integer getTotalMPs() {
        return totalMPs;
    }

    public void setTotalMPs(Integer totalMPs) {
        this.totalMPs = totalMPs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResultPerParty that = (ResultPerParty) o;
        return Objects.equals(id, that.id) && Objects.equals(partyCode, that.partyCode) && Objects.equals(totalVotes, that.totalVotes) && Objects.equals(totalMPs, that.totalMPs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, partyCode, totalVotes, totalMPs);
    }
}
