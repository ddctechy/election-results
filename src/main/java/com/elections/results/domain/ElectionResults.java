package com.elections.results.domain;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "ELECTION_RESULTS",
        uniqueConstraints = { @UniqueConstraint(name = "UC_CP", columnNames = { "constituency", "party_code" }) })
public class ElectionResults {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "entityToSeq")
    @SequenceGenerator(name = "entityToSeq", sequenceName = "SEQ_ELECTION_RESULTS", allocationSize = 1)
    @Column(name = "id")
    private Long id;
    @Column(name = "constituency")
    private String constituency;
    @Column(name = "votes")
    private BigInteger votes;
    @Column(name = "party_code")
    private String partyCode;

    public ElectionResults(){

    }

    public ElectionResults(String constituency, BigInteger votes, String partyCode) {
        this.constituency = constituency;
        this.votes = votes;
        this.partyCode = partyCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConstituency() {
        return constituency;
    }

    public void setConstituency(String constituency) {
        this.constituency = constituency;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public BigInteger getVotes() {
        return votes;
    }

    public void setVotes(BigInteger votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElectionResults that = (ElectionResults) o;
        return Objects.equals(id, that.id) && Objects.equals(constituency, that.constituency) && Objects.equals(partyCode, that.partyCode) && Objects.equals(votes, that.votes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, constituency, partyCode, votes);
    }
}
