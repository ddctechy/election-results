package com.elections.results.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "RESULT_PER_CONSTITUENCY")
public class ResultPerConstituency {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "constituency")
    private String constituency;

    @Column(name = "party_code")
    private String partyCode;

    @Column(name = "votes")
    private BigInteger maxVotes;

    @Column(name = "total")
    private BigInteger totalVotes;

    public ResultPerConstituency() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConstituency() {
        return constituency;
    }

    public void setConstituency(String constituency) {
        this.constituency = constituency;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public BigInteger getMaxVotes() {
        return maxVotes;
    }

    public void setMaxVotes(BigInteger maxVotes) {
        this.maxVotes = maxVotes;
    }

    public BigInteger getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(BigInteger totalVotes) {
        this.totalVotes = totalVotes;
    }
}
