package com.elections.results.domain;

public enum PartyCode {
    C ("CONSERVATIVE PARTY"),
    L ("LABOUR PARTY"),
    UKIP ("UKIP"),
    LD ("LIBERAL DEMOCRATS"),
    G ("GREEN PARTY"),
    IND ("INDEPENDENT"),
    SNP ("SNP");

    public final String partyName;

    PartyCode(String partyName){
        this.partyName = partyName;
    }

    public String getPartyName () {
        return partyName;
    }


}
