package com.elections.results.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * A DTO Class
 */
@Component
public final class Party implements Serializable {

    private String partyName;

    private BigInteger votes;

    public Party() {
    }

    public Party(String partyName, BigInteger votes) {
        this.partyName = partyName;
        this.votes = votes;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public BigInteger getVotes() {
        return votes;
    }

    public void setVotes(BigInteger votes) {
        this.votes = votes;
    }

    private Party(Builder builder) {
        partyName = builder.partyName;
        votes = builder.votes;
    }

    public static Builder newParty(){
        return new Builder();
    }
    public static final class Builder {
        private String partyName;
        private BigInteger votes;

        private Builder(){

        }

        public Party build() {
            return new Party(this);
        }
        public Builder partyName(String partyName) {
            this.partyName = partyName;
            return this;
        }
        public Builder votes(BigInteger votes) {
            this.votes = votes;
            return this;
        }
    }

}
