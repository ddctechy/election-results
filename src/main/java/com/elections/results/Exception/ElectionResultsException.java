package com.elections.results.Exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(value = {
        "cause",
        "stackTrace",
        "suppressed",
        "localizedMessage"
})
public class ElectionResultsException extends RuntimeException{

    @JsonProperty("message")
    private String message;

    public ElectionResultsException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElectionResultsException that = (ElectionResultsException) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }
}
