package com.elections.results.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("v1/api")
@CrossOrigin(origins = "*", maxAge = 3600)
public class BaseController {
}
