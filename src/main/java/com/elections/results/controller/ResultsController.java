package com.elections.results.controller;

import com.elections.results.Exception.ElectionResultsException;
import com.elections.results.service.ElectionResultsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ResultsController extends BaseController{

    private final ElectionResultsService electionResultsService;

    @Autowired
    public ResultsController(ElectionResultsService electionResultsService) {
        this.electionResultsService = electionResultsService;
    }

    @PostMapping(value = "/elections/upload-results", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<String> uploadElectionsResults(@RequestParam(name = "file") MultipartFile results){

        try {
            electionResultsService.processElectionResults(results);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ElectionResultsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping(value = "/elections/get-constituency-results")
    public ResponseEntity<String> getConstituencyResults(){
        try {
            return new ResponseEntity<>(electionResultsService.getElectionResults(), HttpStatus.OK);
        } catch (ElectionResultsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping(value = "/elections/get-total-results")
    public ResponseEntity<String> getTotalResults(){
        try {
            return new ResponseEntity<>(electionResultsService.getTotalResultsPerParty(), HttpStatus.OK);
        } catch (ElectionResultsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

}
