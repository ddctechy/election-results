package com.elections.results.model;

import com.elections.results.domain.Party;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO Class
 */
@Component
public final class ElectionResultsSummary implements Serializable {

    private Long id;

    private String constituency;

    private String votesPercentage;

    private String winner;

    private List<Party> parties;

    public ElectionResultsSummary(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConstituency() {
        return constituency;
    }

    public void setConstituency(String constituency) {
        this.constituency = constituency;
    }

    public String getVotesPercentage() {
        return votesPercentage;
    }

    public void setVotesPercentage(String votesPercentage) {
        this.votesPercentage = votesPercentage;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public List<Party> getParty() {
        return parties;
    }

    public void setParty(List<Party> parties) {
        this.parties = parties;
    }

    public static Builder newElectionResultsSummary(){
        return new Builder();
    }

    public ElectionResultsSummary(Long id, String constituency, String votesPercentage, String winner, List<Party> parties) {
        this.id = id;
        this.constituency = constituency;
        this.votesPercentage = votesPercentage;
        this.winner = winner;
        this.parties = parties;
    }

    private ElectionResultsSummary(Builder builder) {
        id = builder.id;
        constituency = builder.constituency;
        parties = builder.parties;
        votesPercentage = builder.votesPercentage;
        winner = builder.winner;
    }


    public static final class Builder {
        private Long id;
        private String constituency;
        private List<Party> parties;
        private String votesPercentage;
        private String winner;

        private Builder(){

        }

        public ElectionResultsSummary build() {
            return new ElectionResultsSummary(this);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder constituency(String constituency) {
            this.constituency = constituency;
            return this;
        }

        public Builder party(List<Party> parties) {
            this.parties = parties;
            return this;
        }

        public Builder votesPercentage(String votesPercentage) {
            this.votesPercentage = votesPercentage;
            return this;
        }

        public Builder winner(String winner) {
            this.winner = winner;
            return this;
        }
    }
}
