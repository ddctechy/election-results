package com.elections.results.model;

import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class ResultPerPartySummary implements Serializable {

    private String partyName;

    private Integer totalVotes;

    private Integer totalMPs;

    public ResultPerPartySummary() {
    }

    public ResultPerPartySummary(String partyName, Integer totalVotes, Integer totalMPs) {
        this.partyName = partyName;
        this.totalVotes = totalVotes;
        this.totalMPs = totalMPs;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public Integer getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(Integer totalVotes) {
        this.totalVotes = totalVotes;
    }

    public Integer getTotalMPs() {
        return totalMPs;
    }

    public void setTotalMPs(Integer totalMPs) {
        this.totalMPs = totalMPs;
    }

    public static Builder newResultPerPartySummary() {
        return new Builder();
    }

    private ResultPerPartySummary(Builder builder) {
        partyName = builder.partyName;
        totalMPs = builder.totalMPs;
        totalVotes = builder.totalVotes;
    }

    public static final class Builder {
        private String partyName;

        private Integer totalVotes;

        private Integer totalMPs;

        private Builder() {

        }

        public ResultPerPartySummary build() {
            return new ResultPerPartySummary(this);
        }

        public Builder partyName(String partyName) {
            this.partyName = partyName;
            return this;
        }

        public Builder totalVotes(Integer totalVotes) {
            this.totalVotes = totalVotes;
            return this;
        }

        public Builder totalMPs(Integer totalMPs) {
            this.totalMPs = totalMPs;
            return this;
        }

    }

}
