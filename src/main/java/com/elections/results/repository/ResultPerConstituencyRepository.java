package com.elections.results.repository;

import com.elections.results.domain.ResultPerConstituency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultPerConstituencyRepository extends JpaRepository<ResultPerConstituency, Integer> {
}