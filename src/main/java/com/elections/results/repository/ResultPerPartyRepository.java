package com.elections.results.repository;

import com.elections.results.domain.ResultPerParty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultPerPartyRepository extends JpaRepository<ResultPerParty, Integer> {
}
