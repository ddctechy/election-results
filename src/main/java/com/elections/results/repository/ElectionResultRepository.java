package com.elections.results.repository;

import com.elections.results.domain.ElectionResults;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ElectionResultRepository extends JpaRepository<ElectionResults, Integer> {

    @Query(value = "select * from Election_Results where constituency=?1 and party_code=?2",
    nativeQuery = true)
    public Optional<ElectionResults> getElectionResultsByUniqueConstraint(String constituency, String partyCode);
}
