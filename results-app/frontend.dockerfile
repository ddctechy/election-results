FROM node:latest as build

WORKDIR /usr/local/app
COPY ./ /usr/local/app/
RUN npm install
RUN npm run build

FROM nginx:latest

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/local/app/dist/results-app /var/www

EXPOSE 3000

ENTRYPOINT ["nginx","-g","daemon off;"]
