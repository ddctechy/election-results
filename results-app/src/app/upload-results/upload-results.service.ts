import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import {environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class UploadResultsService {

  uploadResultsURL : string = "/v1/api/elections/upload-results";

  constructor(private http: HttpClient) { }

  uploadResults(file: File): Observable<any> {

    const form : FormData = new FormData();
    form.append('file', file);
    return this.http.post(environment.election_service_url + this.uploadResultsURL, form);

  }


}
