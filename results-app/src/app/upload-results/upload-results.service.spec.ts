import { TestBed } from '@angular/core/testing';

import { UploadResultsService } from './upload-results.service';

describe('UploadResultsService', () => {
  let service: UploadResultsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UploadResultsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
