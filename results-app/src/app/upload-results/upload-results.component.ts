import { Component, OnInit } from '@angular/core';
import { UploadResultsService } from './upload-results.service';

@Component({
  selector: 'app-upload-results',
  templateUrl: './upload-results.component.html',
  styleUrls: ['./upload-results.component.css']
})
export class UploadResultsComponent implements OnInit {

  success = false;
  fail = false;
  message : any;
  constructor(private uploadResultsService: UploadResultsService) { }

  ngOnInit(): void {
  }

  upload(event: any){

    const file:File = event.target.files[0];

    this.uploadResultsService.uploadResults(file)
    .subscribe(()=>{
      this.success = true;
      this.fail = false;
    },
    (e) => {
      this.success = false;
      this.fail = true;
      this.message = e.error;
    });

  }

}
