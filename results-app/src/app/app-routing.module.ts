import { HttpClientModule } from '@angular/common/http';
import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowResultsSummaryComponent } from './show-results-summary/show-results-summary.component';
import { UploadResultsComponent } from './upload-results/upload-results.component';

const routes: Routes = [
 {path:'', component: UploadResultsComponent},
 {path:'results', component: ShowResultsSummaryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
  HttpClientModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
