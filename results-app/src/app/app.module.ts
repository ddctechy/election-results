import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UploadResultsComponent } from './upload-results/upload-results.component';
import { ShowResultsSummaryComponent } from './show-results-summary/show-results-summary.component';
import { UploadResultsService } from './upload-results/upload-results.service';
import { ShowResultsSummaryService } from './show-results-summary/show-results-summary.service';

@NgModule({
  declarations: [
    AppComponent,
    UploadResultsComponent,
    ShowResultsSummaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [UploadResultsService,
              ShowResultsSummaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
