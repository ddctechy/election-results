import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowResultsSummaryComponent } from './show-results-summary.component';

describe('ShowResultsSummaryComponent', () => {
  let component: ShowResultsSummaryComponent;
  let fixture: ComponentFixture<ShowResultsSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowResultsSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowResultsSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
