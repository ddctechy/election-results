import { TestBed } from '@angular/core/testing';

import { ShowResultsSummaryService } from './show-results-summary.service';

describe('ShowResultsSummaryService', () => {
  let service: ShowResultsSummaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShowResultsSummaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
