import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import {environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ShowResultsSummaryService {

  getResultsSummaryURL : string = "/v1/api/elections/get-constituency-results";
  getPartyResultsSummaryURL : string = "/v1/api/elections/get-total-results";

  constructor(private http: HttpClient) { }

  getResultsSummary() : Observable<any>{

    console.log("displaySummary !!");

    return this.http.get(environment.election_service_url + this.getResultsSummaryURL);

  }

  getPartyResultsSummary() : Observable<any>{

    console.log("displayPartySummary !!");

    return this.http.get(environment.election_service_url + this.getPartyResultsSummaryURL);

  }
}
