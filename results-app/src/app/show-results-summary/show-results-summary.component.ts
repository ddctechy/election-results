import { Component, OnInit } from '@angular/core';
import {ShowResultsSummaryService} from './show-results-summary.service';

@Component({
  selector: 'app-show-results-summary',
  templateUrl: './show-results-summary.component.html',
  styleUrls: ['./show-results-summary.component.css']
})
export class ShowResultsSummaryComponent implements OnInit {

  constResult: any;
  partyResult: any;
  failC = false;
  failP = false;
  constResultMessage : any;
  partyResultMessage : any;
  showConstResult = false;
  showPartyResult = false;

  constructor(private showResultsSummaryService : ShowResultsSummaryService) { }

  ngOnInit(): void {
  }

  displaySummary() {
    this.showConstResult = true;
    this.showPartyResult = false;
    this.showResultsSummaryService.getResultsSummary().subscribe(
    (results:any)=> {
          this.failC = false;
          this.constResult=results;

    },
    (e) => {
          this.failC = true;
          this.constResultMessage = e.error;
    });

  }

  displayPartySummary() {
    this.showPartyResult = true;
    this.showConstResult = false;
    this.showResultsSummaryService.getPartyResultsSummary().subscribe(
    (results:any)=> {
          this.failP = false;
          this.partyResult=results;
        },
        (e) => {
          this.failP = true;
          this.partyResultMessage = e.error;
        }
    );


  }

}
